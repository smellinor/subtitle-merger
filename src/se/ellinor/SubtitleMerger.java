package se.ellinor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import se.ellinor.model.Line;
import se.ellinor.model.MergedSubtitleLines;
import se.ellinor.model.SubtitleLine;
import se.ellinor.model.TimeStamp;

public class SubtitleMerger {

	private BufferedReader firstFileBR;
	private BufferedReader secondFileBR;

	public SubtitleMerger() {

	}

	public File merge(File first, File second) throws IOException {
		try {
			this.firstFileBR = new BufferedReader(new FileReader(first));
			this.secondFileBR = new BufferedReader(new FileReader(second));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		String path = first.getAbsolutePath();
		return this.mergeFiles(path.substring(0, path.length() -4 -1));
	}

	private File mergeFiles(String path) throws IOException {
		File result = new File(path + "_MERGED.srt");

		mergeLinesIntoFile(mergeSubtitles(getLines(this.firstFileBR), getLines(this.secondFileBR)), result);
		try {
			this.firstFileBR.close();
			this.secondFileBR.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<MergedSubtitleLines> mergeSubtitles(List<SubtitleLine> first, List<SubtitleLine> second) {
		List<MergedSubtitleLines> merged = new ArrayList<>();
		for(int i = 0; i < first.size(); i++) {
			MergedSubtitleLines msl = new MergedSubtitleLines();
			msl.setFirstLine(first.get(i));
			merged.add(msl);
		}
		for(int i = 0; i < second.size(); i++) {
			merged.get(i).setSecondLine(second.get(i));
		}
		return merged;
	}

	public void mergeLinesIntoFile(List<MergedSubtitleLines> lines, File result) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(result));
		for(int i = 0; i < lines.size(); i++) {
			bw.write(lines.get(i).getText() + "\n\n");
		}
		bw.close();
	}

	public List<SubtitleLine> getLines(BufferedReader br) throws IOException {
		List<SubtitleLine> lines = new ArrayList<>();
		int lineNr = 1;
		int nrLines = 0;
		int loops = 0;
		String line = br.readLine();
		while(line != null) {
			if(loops % 5 == 0) {
				SubtitleLine sl = new SubtitleLine();
				sl.setNumber(nrLines +1);
				lines.add(sl);
				nrLines++;
			} else if(line.isBlank()) {
				lineNr = 1;
				loops = -1;
			} else if(TimeStamp.form(line)) {
				lines.get(lines.size()-1).setTimeStamp(new TimeStamp(line)); 
			} else if (lineNr == 1){
				lines.get(lines.size()-1).setFirstLine(new Line(line));
				lineNr = 2;
			} else if(lineNr == 2) {
				lines.get(lines.size()-1).setSecondLine(new Line(line));
				lineNr = 1;
			}
			loops++;
			line = br.readLine();
		}

		return lines;
	}



}
