package se.ellinor.model;

public class Line implements Text {
	
	private String text;
	
	public Line() {
		this("");
	}
	
	public Line(String text) {
		this.text = new String(text);
	}
	
	public Line(Line l) {
		this(l.getText());
	}
	
	@Override
	public void setText(String text) {
		this.text = new String(text);
	}
	
	@Override
	public String getText() {
		return new String(this.text);
	}
	
	

}
