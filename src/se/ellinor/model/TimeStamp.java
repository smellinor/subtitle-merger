package se.ellinor.model;

public class TimeStamp implements Text {
	
	private String timestamp;
	
	public TimeStamp() {
		this("");
	}
	
	public TimeStamp(String text) {
		this.timestamp = new String(text);
	}
	
	public TimeStamp(TimeStamp ts) {
		this(ts.getText());
	}
	
	@Override
	public void setText(String text) {
		this.timestamp = new String(text);
	}
	
	@Override
	public String getText() {
		return new String(this.timestamp);
	}

	public static boolean form(String ts) {
		char[] tsChars = ts.toCharArray();
		
		for(int i = 0 ; i < tsChars.length ; i++) {
			if(tsChars[i] == '-') {
				if(tsChars[i+1] == '-')
					if(tsChars[i+2] == '>')
						return true;
			}
		}
		
		return false;
	}
	
}
