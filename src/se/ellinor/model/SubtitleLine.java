package se.ellinor.model;

public class SubtitleLine implements Text {
	
	private Line firstLine;
	private Line secondLine;
	private TimeStamp timeStamp;
	private int number;
	
	public SubtitleLine() {
		this(new Line(), new Line(), new TimeStamp(), 0);
	}
	
	public SubtitleLine(Line first, Line second, TimeStamp ts, int n) {
		this.firstLine = new Line(first);
		this.secondLine = new Line(second);
		this.timeStamp = new TimeStamp(ts);
		this.number = n;
	}
	
	public SubtitleLine(SubtitleLine sl) {
		this(sl.getFirstLine(), sl.getSecondLine(), sl.getTimeStamp(), sl.getNumber());
	}
	
	public Line getFirstLine() {
		return new Line(this.firstLine);
	}
	
	public Line getSecondLine() {
		return new Line(this.secondLine);
	}
	
	public TimeStamp getTimeStamp() {
		return new TimeStamp(this.timeStamp);
	}
	
	public int getNumber() {
		return this.number;
	}
	
	public void setFirstLine(Line line) {
		this.firstLine = new Line(line);
	}
	
	public void setSecondLine(Line line) {
		this.secondLine = new Line(line);
	}
	
	public void setTimeStamp(TimeStamp ts) {
		this.timeStamp = new TimeStamp(ts);
	}
	
	public void setNumber(int n) {
		this.number = n;
	}

	@Override
	public void setText(String text) {
		String[] textStrings = text.split("\n");
		this.firstLine = new Line(textStrings[0]);
		this.secondLine = new Line(textStrings[1]);
		this.timeStamp = new TimeStamp(textStrings[2]);
		this.number = Integer.parseInt(textStrings[3]);
	}

	@Override
	public String getText() {
		return String.format("%d\n%s\n%s\n%s", this.number, this.timeStamp.getText(), this.firstLine.getText(), this.secondLine.getText());
	}
	
	

}
