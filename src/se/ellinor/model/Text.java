package se.ellinor.model;

public interface Text {
	
	public void setText(String text);
	public String getText();

}
