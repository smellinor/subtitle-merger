package se.ellinor.model;

public class MergedSubtitleLines implements Text {

	private SubtitleLine firstLine;
	private SubtitleLine secondLine;

	public MergedSubtitleLines() {
		this(new SubtitleLine(new Line(""), new Line(""), new TimeStamp(""), 0), new SubtitleLine(new Line(""), new Line(""), new TimeStamp(""), 0));
	}

	public MergedSubtitleLines(SubtitleLine l1, SubtitleLine l2) {
		this.firstLine = new SubtitleLine(l1);
		this.secondLine = new SubtitleLine(l2);
	}

	public MergedSubtitleLines(MergedSubtitleLines msl) {
		this(msl.getFirstLine(), msl.getSecondLine());
	}
	
	public void setFirstLine(SubtitleLine l) {
		this.firstLine = new SubtitleLine(l);
	}
	
	public void setSecondLine(SubtitleLine l) {
		this.secondLine = new SubtitleLine(l);
	}
	
	public SubtitleLine getSecondLine() {
		return new SubtitleLine(this.secondLine);
	}
	
	public SubtitleLine getFirstLine() {
		return new SubtitleLine(this.firstLine);
	}
	
	@Override
	public void setText(String text) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getText() {
		return String.format("%d\n%s\n%s\n%s\n%s\n%s", 
				this.firstLine.getNumber(), 
				this.firstLine.getTimeStamp().getText(), 
				this.firstLine.getFirstLine().getText(), 
				this.secondLine.getFirstLine().getText(), 
				this.firstLine.getSecondLine().getText(), 
				this.secondLine.getSecondLine().getText());
	}

}
