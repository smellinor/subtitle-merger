package se.ellinor;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Main extends JFrame {

	private static final long serialVersionUID = 1L;
	private final int SIZE = 500;
	private JFileChooser fc;
	private SubtitleMerger sm;
	private File firstFile;
	private File secondFile;

	public Main() {
		super();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(SIZE, SIZE);
		this.setVisible(true);
		this.fc = new JFileChooser();

		this.add(createPanel());

		this.sm = new SubtitleMerger();
	}

	private JPanel createPanel() {
		JPanel panel = new JPanel(new GridLayout(4,1));
		panel.add(createOpenFirstFileButton(panel));
		panel.add(createOpenSecondFileButton(panel));
		panel.add(createMergeFilesButton(panel));
		return panel;
	}

	private JButton createMergeFilesButton(JPanel panel) {
		JButton button = new JButton("Merge files");
		button.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							File f = sm.merge(firstFile, secondFile);
							panel.add(createLabel(f.getAbsolutePath()));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}});
		return button;
	}

	private JButton createOpenSecondFileButton(JPanel panel) {
		JButton button = new JButton("Choose second file");
		button.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						int returnVal = fc.showOpenDialog(panel);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							secondFile = fc.getSelectedFile();
						}
					}});
		return button;
	}

	private JButton createOpenFirstFileButton(JPanel panel) {
		JButton button = new JButton("Choose first file");
		button.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						int returnVal = fc.showOpenDialog(panel);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							firstFile = fc.getSelectedFile();
						}
					}});
		return button;
	}

	private JLabel createLabel(String label) {
		return new JLabel(label);
	}

	public static void main(String[] args) {
		new Main();
	}

}
